#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void DisplayMadlib(string inputs[], ostream &out) {
	
	out << "The man went into the " << inputs[0] << " house. \n";
	out << "In the dining room he found a " << inputs[1] << ". \n";
	out << "Then he " << inputs[2] << " to the basement where he\n";
	out << "broke " << inputs[1] << " while " << inputs[3] << " down the stairs. \n";
	out << "The " << inputs[4] << " basement was very dark. \n";
	out << "Out jumped a " << inputs[5] << ". \n";
	out << "He struck the " << inputs[5]  << " with the " << inputs[6] << ". \n";
	out << "In the basement, he found a portal to " << inputs[7] << ". \n";
	out << inputs[8] << " was on the other side of the portal. \n";
	out << "It turned out "<< inputs[8] <<" giving out free " << inputs[9] << ". \n";
};

int main() {

	string filepath = "C:\\temp\\madlib.txt";
	string inputs[10] = {
		"n adjective",
		" noun",
		" verb",
		" verb",
		"n adjective",
		" noun",
		"n object",
		" place",
		" person",
		" food"
	};


	for (int i = 0; i < 10; i++) {
		cout << "Enter a" << inputs[i] << ": ";
		getline(cin, inputs[i]);
	}

	cout << "\n";
	DisplayMadlib(inputs, cout);

	cout << "\nWould you like to save(Y)(N): ";
	char choice;
	cin >> choice;

	if (choice == 'Y' || choice == 'y') {
		ofstream fout(filepath);
		if (fout)
			DisplayMadlib(inputs, fout);
		fout.close();
	}


	_getch();
	return 0;
}